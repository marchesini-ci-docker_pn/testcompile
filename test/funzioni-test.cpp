#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "../src/funzioni.h"

TEST_CASE("Pow Test", "[0]"){
    REQUIRE(pow(2) == 4);
    REQUIRE(pow(3) == 9);
    REQUIRE(pow(4) == 16);
}